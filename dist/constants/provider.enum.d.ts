export declare enum ProviderEnum {
    INFO_AUTH = "infoauth",
    AWS = "aws"
}
export declare const parseProvider: (provider: string) => ProviderEnum;
//# sourceMappingURL=provider.enum.d.ts.map