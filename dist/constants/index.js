"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.PROVIDER = 'provider';
exports.TOKEN_KEY = 'authorization';
exports.REFRESH_TOKEN_KEY = 'refresh-token';
var provider_enum_1 = require("./provider.enum");
exports.ProviderEnum = provider_enum_1.ProviderEnum;
exports.parseProvider = provider_enum_1.parseProvider;
var error_reason_enum_1 = require("./error.reason.enum");
exports.ErrorReasonEnum = error_reason_enum_1.ErrorReasonEnum;
exports.SECRET = 'secret';
exports.USER_DETAILS_KEY = 'userDetails';
//# sourceMappingURL=index.js.map