"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var ProviderEnum;
(function (ProviderEnum) {
    ProviderEnum["INFO_AUTH"] = "infoauth";
    ProviderEnum["AWS"] = "aws";
})(ProviderEnum = exports.ProviderEnum || (exports.ProviderEnum = {}));
exports.parseProvider = (provider) => {
    switch (provider) {
        case ProviderEnum.INFO_AUTH:
            return ProviderEnum.INFO_AUTH;
        case ProviderEnum.AWS:
            return ProviderEnum.AWS;
        default:
            return null;
    }
};
//# sourceMappingURL=provider.enum.js.map