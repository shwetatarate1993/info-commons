export declare const PROVIDER: string;
export declare const TOKEN_KEY: string;
export declare const REFRESH_TOKEN_KEY: string;
export { ProviderEnum, parseProvider } from './provider.enum';
export { ErrorReasonEnum } from './error.reason.enum';
export declare const SECRET: string;
export declare const USER_DETAILS_KEY: string;
//# sourceMappingURL=index.d.ts.map