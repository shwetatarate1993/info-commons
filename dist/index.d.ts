export declare const INFO_META_URI = "http://ec2-3-87-133-52.compute-1.amazonaws.com:8890/graphql";
export declare const APPENG_META_URI = "http://ec2-34-201-146-30.compute-1.amazonaws.com:8897/graphql";
export declare const APPENG_OUTBOUND_URI = "http://ec2-3-87-133-52.compute-1.amazonaws.com:8898";
export declare const DIALECT = "sqlite3";
export declare let APP_ENV: string;
export { validateToken } from './validators';
export { ErrorReasonEnum, PROVIDER, SECRET, TOKEN_KEY, USER_DETAILS_KEY } from './constants';
export { Errors, ErrorResponse, PhysicalEntity, PhysicalColumn } from './models';
export { createApolloClient, errorResponse, getMetaData, putMissingdbCodesinParams, isExpressionResolved, queryExtractor, queryParser, queryResolver, extractLabel, format, parse, } from './utilities';
export { default as config } from './config';
//# sourceMappingURL=index.d.ts.map