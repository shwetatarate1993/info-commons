import { ErrorResponse } from '../models';
export default interface Validator {
    validateToken(token: string): Promise<ErrorResponse>;
}
//# sourceMappingURL=validator.interface.d.ts.map