import Validator from './validator.interface';
export { default as awsValidator } from './aws.validator';
export { default as infoAuthValidator } from './infoauth.validator';
export { default as validateToken } from './validation';
export declare const getProviderValidator: (provider: string) => Validator;
//# sourceMappingURL=index.d.ts.map