import { ErrorResponse } from '../models';
import Validator from './validator.interface';
declare class InfoAuthValidator implements Validator {
    validateToken(token: string): Promise<ErrorResponse>;
}
declare const _default: InfoAuthValidator;
export default _default;
//# sourceMappingURL=infoauth.validator.d.ts.map