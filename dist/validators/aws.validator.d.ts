import { ErrorResponse } from '../models';
import Validator from './validator.interface';
declare class AwsValidator implements Validator {
    validateToken(token: string): Promise<ErrorResponse>;
}
declare const _default: AwsValidator;
export default _default;
//# sourceMappingURL=aws.validator.d.ts.map