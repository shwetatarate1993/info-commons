"use strict";
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const jwt = __importStar(require("jsonwebtoken"));
const config_1 = __importDefault(require("../config"));
const constants_1 = require("../constants");
const models_1 = require("../models");
class InfoAuthValidator {
    async validateToken(token) {
        if (!token) {
            const error = new models_1.ErrorResponse();
            error.code = 401;
            error.message = 'Token is missing';
            error.errors = [{ domain: 'global', reason: constants_1.ErrorReasonEnum.MISSING_TOKEN, message: 'Token is required', locationType: 'Request Header',
                    location: 'x-access-token', extendedHelp: 'http://request/help' }];
            return error;
        }
        try {
            jwt.verify(token, config_1.default.get(constants_1.SECRET));
        }
        catch (err) {
            const error = new models_1.ErrorResponse();
            error.code = 401;
            error.message = 'Token is invalid';
            error.errors = [{ domain: 'global', reason: constants_1.ErrorReasonEnum.INVALID_TOKEN, message: 'Please Send a Valid Token', locationType: 'Request Header',
                    location: 'x-access-token', extendedHelp: 'http://request/help' }];
            return error;
        }
        return null;
    }
}
exports.default = new InfoAuthValidator();
//# sourceMappingURL=infoauth.validator.js.map