import { ErrorResponse } from '../models';
declare const validateToken: (jsonToken: string, provider: string) => Promise<ErrorResponse>;
export default validateToken;
//# sourceMappingURL=validation.d.ts.map