"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const _1 = require(".");
const constants_1 = require("../constants");
const models_1 = require("../models");
const validators_1 = require("../validators");
const validateToken = async (jsonToken, provider) => {
    let errorResponse;
    if (provider === undefined) {
        errorResponse = await validators_1.infoAuthValidator.validateToken(jsonToken);
    }
    else {
        const parsedProvider = constants_1.parseProvider(provider);
        if (parsedProvider === null) {
            return await createInvalidProviderError(provider);
        }
        errorResponse = await _1.getProviderValidator(provider).validateToken(jsonToken);
    }
    return errorResponse;
};
const createInvalidProviderError = async (provider) => {
    const error = new models_1.ErrorResponse();
    error.code = 401;
    error.message = 'Invalid Provider';
    error.errors = [{ domain: 'global', reason: constants_1.ErrorReasonEnum.INVALID_PROVIDER,
            message: provider + ' is a invalid provider. Allowed values are [infoauth, aws]',
            locationType: 'Request Header', location: 'provider',
            extendedHelp: 'http://request/help' }];
    return error;
};
exports.default = validateToken;
//# sourceMappingURL=validation.js.map