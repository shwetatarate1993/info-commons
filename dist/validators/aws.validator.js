"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const jsonwebtoken_1 = __importDefault(require("jsonwebtoken"));
const jwk_to_pem_1 = __importDefault(require("jwk-to-pem"));
const config_1 = __importDefault(require("../config"));
const constants_1 = require("../constants");
const models_1 = require("../models");
class AwsValidator {
    async validateToken(token) {
        if (!token) {
            const error = new models_1.ErrorResponse();
            error.code = 401;
            error.message = 'Token is missing';
            error.errors = [{ domain: 'global', reason: constants_1.ErrorReasonEnum.MISSING_TOKEN, message: 'Token is required', locationType: 'Request Header',
                    location: 'x-access-token', extendedHelp: 'http://request/help' }];
            return error;
        }
        const pem = jwk_to_pem_1.default(config_1.default.get('jwk'));
        try {
            jsonwebtoken_1.default.verify(token, pem, { algorithms: ['RS256'] });
        }
        catch (err) {
            const error = new models_1.ErrorResponse();
            error.code = 401;
            error.message = 'Token is invalid';
            error.errors = [{ domain: 'global', reason: constants_1.ErrorReasonEnum.INVALID_TOKEN, message: 'Please Send a Valid Token', locationType: 'Request Header',
                    location: 'x-access-token', extendedHelp: 'http://request/help' }];
            return error;
        }
        return null;
    }
}
exports.default = new AwsValidator();
//# sourceMappingURL=aws.validator.js.map