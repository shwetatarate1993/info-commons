"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const _1 = require(".");
const constants_1 = require("../constants");
var aws_validator_1 = require("./aws.validator");
exports.awsValidator = aws_validator_1.default;
var infoauth_validator_1 = require("./infoauth.validator");
exports.infoAuthValidator = infoauth_validator_1.default;
var validation_1 = require("./validation");
exports.validateToken = validation_1.default;
exports.getProviderValidator = (provider) => {
    switch (provider) {
        case constants_1.ProviderEnum.INFO_AUTH:
            return _1.infoAuthValidator;
        case constants_1.ProviderEnum.AWS:
            return _1.awsValidator;
    }
};
//# sourceMappingURL=index.js.map