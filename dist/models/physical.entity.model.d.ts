import PhysicalColumn from './physical.column.model';
export default class PhysicalEntity {
    readonly configObjectId: string;
    readonly name: string;
    readonly configObjectType: string;
    readonly createdBy: string;
    readonly isDeleted: number;
    readonly itemDescription: string;
    readonly creationDate: Date;
    readonly projectId: number;
    readonly updatedBy: string;
    readonly updationDate: Date;
    readonly deletionDate: Date;
    readonly schemaName: string;
    readonly dbTypeName: string;
    readonly dbtype: string;
    readonly isVerticalEntity: boolean;
    readonly order: number;
    readonly singleSelectQID: string;
    readonly multiSelectQID: string;
    readonly updateQID: string;
    readonly deleteQID: string;
    readonly insertQID: string;
    readonly sequenceQID: string;
    readonly workAreaSessName: string;
    readonly releaseAreaSqlSessionfactoryName: string;
    readonly expressionAvailable: boolean;
    readonly accessibilityRegex: string;
    readonly isMultivalueMapping: boolean;
    readonly physicalColumns: PhysicalColumn[];
    getPrimaryKeyColumn: () => PhysicalColumn;
    getPropertyMap: () => {
        dbCodeList: string[];
        physicalColumnMandatoryList: string[];
    };
}
//# sourceMappingURL=physical.entity.model.d.ts.map