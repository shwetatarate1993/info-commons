declare class Errors {
    domain?: string;
    reason?: string;
    message?: string;
    locationType?: string;
    location?: string;
    extendedHelp?: string;
}
export default Errors;
//# sourceMappingURL=errors.model.d.ts.map