import { Errors } from './';
export default class ErrorResponse {
    code: number;
    message: string;
    errors?: Errors[] | null;
}
//# sourceMappingURL=errorresponse.model.d.ts.map