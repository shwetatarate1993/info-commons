"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const expression_resolver_1 = require("./expression.resolver");
exports.queryParser = (query, params) => {
    let queryString = '';
    for (const temp of query.split('</if>')) {
        let regex = '';
        let b = '';
        for (let temp1 of temp.split('<if')) {
            if (temp1.includes('test="') && temp.includes('">')) {
                if (regex === '') {
                    temp1 = temp1.replace('test="', '#{');
                }
                else {
                    temp1 = temp1.replace('test="', '');
                }
                const regexSplitterIndex = temp1.indexOf('">');
                b = temp1.substring(regexSplitterIndex + 2);
                temp1 = temp1.replace('">', '}');
                if (b !== '') {
                    temp1 = temp1.replace(b, '');
                }
                const regexStartIndex = 0;
                const regexEndIndex = temp1.length;
                if (regex !== '') {
                    regex = regex.replace('}', '').concat(' and ');
                }
                regex = regex.concat(temp1.substring(regexStartIndex, regexEndIndex));
            }
            else {
                queryString = queryString + temp1;
            }
        }
        if (regex !== '') {
            regex = regex.trim();
            const dbcodeExtractor = regex;
            let dbcode = '';
            if (dbcodeExtractor.includes('#{')) {
                const db = dbcodeExtractor.replace('#{', '');
                for (const dbCode of db.split('and|or')) {
                    if (dbCode.includes('==')) {
                        dbcode = dbCode.substring(0, dbCode.indexOf('='));
                    }
                    else if (dbCode.includes('!=')) {
                        dbcode = dbCode.substring(0, dbCode.indexOf('!'));
                    }
                    else if (dbCode.includes('<')) {
                        dbcode = dbCode.substring(0, dbCode.indexOf('<'));
                    }
                    else if (dbCode.includes('>')) {
                        dbcode = dbCode.substring(0, dbCode.indexOf('>'));
                    }
                    if (!(dbcode.toString().trim() in params)) {
                        params[dbcode.toString().trim()] = null;
                    }
                }
            }
            if (expression_resolver_1.isExpressionResolved(regex, params)) {
                queryString = queryString + ' ';
                queryString = queryString + b;
            }
        }
    }
    return queryString;
};
//# sourceMappingURL=query.parser.js.map