import { ErrorResponse } from '../models';
declare const errorResponse: (errorCode: number, errorMessage: string, domain: string, reason: string, message: string, locationType: string, location: string, extendedHelp: string) => ErrorResponse;
export default errorResponse;
//# sourceMappingURL=error.response.util.d.ts.map