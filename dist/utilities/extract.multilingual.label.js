"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.extractLabel = ((configLabel, locale) => {
    let label = configLabel;
    if (configLabel) {
        if (configLabel.includes('-!sEpArAToR!-')) {
            const labelWithLang = configLabel.split('-!sEpArAToR!-');
            labelWithLang.some((item) => {
                const data = item.split('-!lAnGuAge!-');
                if (data[0] === locale) {
                    label = data[1];
                    return true;
                }
                else {
                    return false;
                }
            });
            if (label.includes('-!sEpArAToR!-')) {
                label = labelWithLang[0].split('-!lAnGuAge!-')[1];
            }
        }
        else if (configLabel.includes('-!lAnGuAge!-')) {
            label = configLabel.split('-!lAnGuAge!-')[1];
        }
        return label;
    }
    return configLabel;
});
//# sourceMappingURL=extract.multilingual.label.js.map