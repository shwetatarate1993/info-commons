"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const _1 = require("./");
const getMetaData = async (uri, id, query, token) => {
    const client = _1.createApolloClient(uri);
    const { data } = await client.query({
        query,
        context: {
            headers: {
                TOKEN_KEY: token,
            },
        },
        variables: {
            id,
        },
    });
    return data;
};
exports.default = getMetaData;
//# sourceMappingURL=metadata.util.js.map