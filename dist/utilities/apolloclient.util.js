"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const apollo_client_1 = require("apollo-client");
const apollo_link_http_1 = require("apollo-link-http");
/* tslint:disable-next-line:no-var-requires */
const fetch = require('node-fetch');
const apollo_cache_inmemory_1 = require("apollo-cache-inmemory");
const createApolloClient = (uri) => {
    const client = new apollo_client_1.ApolloClient({
        ssrMode: true,
        link: new apollo_link_http_1.HttpLink({
            uri,
            fetch,
        }),
        cache: new apollo_cache_inmemory_1.InMemoryCache(),
    });
    return client;
};
exports.default = createApolloClient;
//# sourceMappingURL=apolloclient.util.js.map