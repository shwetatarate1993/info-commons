"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.isExpressionResolved = (expression, input) => {
    if (expression) {
        expression = manipulateExpression(expression, input);
        try {
            /* tslint:disable-next-line:no-eval */
            return eval(expression);
        }
        catch (e) {
            return false;
        }
    }
    else {
        return true;
    }
};
const manipulateExpression = (expression, input) => {
    const regexAnd = new RegExp('\\b' + 'and' + '\\b', 'ig');
    const regexOr = new RegExp('\\b' + 'or' + '\\b', 'ig');
    if (expression.startsWith('JAVASCRIPT$$')) {
        expression = expression.substring(expression.indexOf('$') + 2);
        expression = expression.replace(regexAnd, '&&').replace(regexOr, '||');
        return expression;
    }
    else {
        expression = expression.replace(/#/, '').replace(/{/, '').replace(/}/, '')
            .replace(regexAnd, '&&').replace(regexOr, '||');
        Object.entries(input).forEach(([key, value]) => {
            if (key !== '' && key !== undefined) {
                const reg = new RegExp('\\b' + key + '\\b', 'g');
                const valueConcat = 'input.' + key;
                expression = expression.replace(reg, valueConcat);
            }
        });
        return expression;
    }
};
//# sourceMappingURL=expression.resolver.js.map