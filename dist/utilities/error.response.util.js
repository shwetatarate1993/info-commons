"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const models_1 = require("../models");
const errorResponse = (errorCode, errorMessage, domain, reason, message, locationType, location, extendedHelp) => {
    const error = new models_1.ErrorResponse();
    error.code = errorCode;
    error.message = errorMessage;
    error.errors = [{
            domain, reason, message, locationType,
            location, extendedHelp,
        }];
    return error;
};
exports.default = errorResponse;
//# sourceMappingURL=error.response.util.js.map