import { ApolloClient } from 'apollo-client';
declare const createApolloClient: (uri: string) => ApolloClient<unknown>;
export default createApolloClient;
//# sourceMappingURL=apolloclient.util.d.ts.map