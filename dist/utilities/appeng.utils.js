"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const cloneDeep_1 = __importDefault(require("lodash/cloneDeep"));
exports.putMissingdbCodesinParams = (query, inParams) => {
    let dbCodeList = [];
    const params = cloneDeep_1.default(inParams);
    dbCodeList = query.match(/:\w+/g);
    if (dbCodeList !== null) {
        dbCodeList.map((dbCode) => {
            const column = dbCode.replace(':', '').trim();
            if (params[column] === undefined) {
                params[column] = null;
            }
        });
    }
    return params;
};
//# sourceMappingURL=appeng.utils.js.map