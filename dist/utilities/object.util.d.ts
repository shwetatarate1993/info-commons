import _ from 'lodash';
export declare const parse: (response: object) => _.Dictionary<never>;
export declare const format: (attributes: object) => _.Dictionary<never>;
//# sourceMappingURL=object.util.d.ts.map