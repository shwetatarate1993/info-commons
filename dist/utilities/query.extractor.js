"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.queryExtractor = (inQuery, dialect) => {
    let extractedQuery = inQuery !== null ? inQuery : '';
    if (extractedQuery !== '') {
        if (extractedQuery.includes('sEpArAToR')) {
            const queryWithDialect = extractedQuery.split('sEpArAToR');
            queryWithDialect.some((item) => {
                const data = item.split('!lAnGuAge!');
                if ((data[0] === dialect) || (data[0] === 'derby' && dialect === 'sqlite3')) {
                    extractedQuery = data[1];
                    return true;
                }
            });
        }
        else if (extractedQuery.includes('!lAnGuAge!')) {
            const data = extractedQuery.split('!lAnGuAge!');
            extractedQuery = data[1];
        }
        return extractedQuery;
    }
    return extractedQuery;
};
//# sourceMappingURL=query.extractor.js.map