"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const lodash_1 = __importDefault(require("lodash"));
exports.parse = (response) => {
    return lodash_1.default.mapKeys(response, (value, key) => {
        return lodash_1.default.camelCase(key);
    });
};
exports.format = (attributes) => {
    return lodash_1.default.mapKeys(attributes, (value, key) => {
        return lodash_1.default.snakeCase(key);
    });
};
//# sourceMappingURL=object.util.js.map