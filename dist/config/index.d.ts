import { default as convict } from 'convict';
declare const config: convict.Config<{
    secret: string;
    jwk: {
        alg: string;
        e: string;
        kid: string;
        kty: string;
        n: string;
        use: string;
    };
    db: {
        mock: any;
        PRIMARYSPRING: any;
        PRIMARY_MD_SPRING: any;
        METADB: any;
        SPRING_INBOX_FM: any;
    };
    links: {
        appeng_meta: any;
    };
}>;
export default config;
//# sourceMappingURL=index.d.ts.map