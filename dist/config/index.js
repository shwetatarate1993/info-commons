"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const convict_1 = __importDefault(require("convict"));
const __1 = require("../");
const config = convict_1.default({
    secret: {
        doc: 'Secret key',
        format: '*',
        default: 'defaultsecret',
        env: 'SECRET_KEY',
    },
    jwk: {
        doc: 'jwk-pem',
        format: '*',
        default: {
            alg: 'RS256',
            e: 'AQAB',
            kid: '/6MdMET5r2hYwQC1X7rHaMk/aeiJi0e3xYJifclIccM=',
            kty: 'RSA',
            n: 'gyHQAXjgC4mWxe0uw76GdIGr3-Q-P4_0PA935bLWgX_XxqquUhtWpYvl3NqCFpu32a-wkqh6euiv36k'
                + '4aTEcNF3d7ccmNLAOcAo6-URBDuLyDgtTTWDp57DTNoiSwliKMDsOI2_kY3alsh-4OQBMttcUvVxFGdHbm'
                + 'sKZ-K-9fjqEfOQpXCUv_BSNDGgKscHQU_aRaA05s1r5ME4Sdz3KIj8ew7sp-PKNGdoe5T6s1vmug5jUwMgn'
                + 'pqKsEmJOmYmpWh668Z-CRbjHVXfK2hsNPPGALK8TIiRmNtsUdDwzmPJiezPOqV0vML4DMS_EKkbD8pylbhxC'
                + 'eWjz4ioEPLe_jw',
            use: 'sig',
        },
        env: 'JWK_PEM',
    },
    db: {
        mock: {
            sqlite3: {
                dialect: 'sqlite3',
                connection: {
                    doc: 'SQL Lite connection type',
                    format: '*',
                    default: ':memory:',
                },
            },
        },
        PRIMARYSPRING: {
            mysql: {
                dialect: 'mysql',
                host: {
                    doc: 'Database host name/IP',
                    format: '*',
                    default: 'awsmysqldev1.ckb0c9p1c3xw.us-east-1.rds.amazonaws.com',
                },
                name: {
                    doc: 'Database name',
                    format: String,
                    default: 'villageportalappdev',
                },
                username: {
                    doc: 'Database Username',
                    format: String,
                    default: 'awsdevmaster',
                },
                password: {
                    doc: 'Database Password',
                    format: String,
                    default: 'awsinfo2018',
                },
                pool: {
                    max: 5,
                    min: 0,
                    acquire: 30000,
                    idle: 10000,
                },
            },
            sqlite3: {
                dialect: 'sqlite3',
                connection: {
                    filename: 'vpdata.db',
                },
            },
        },
        PRIMARY_MD_SPRING: {
            mysql: {
                dialect: 'mysql',
                host: {
                    doc: 'Database host name/IP',
                    format: '*',
                    default: 'awsmysqldev1.ckb0c9p1c3xw.us-east-1.rds.amazonaws.com',
                },
                name: {
                    doc: 'Database name',
                    format: String,
                    default: 'vpmd',
                },
                username: {
                    doc: 'Database Username',
                    format: String,
                    default: 'awsdevmaster',
                },
                password: {
                    doc: 'Database Password',
                    format: String,
                    default: 'awsinfo2018',
                },
                pool: {
                    max: 5,
                    min: 0,
                    acquire: 30000,
                    idle: 10000,
                },
            },
            sqlite3: {
                dialect: 'sqlite3',
                connection: {
                    filename: 'vpdata.db',
                },
            },
        },
        METADB: {
            mysql: {
                dialect: 'mysql',
                host: {
                    doc: 'Database host name/IP',
                    format: '*',
                    default: 'awsmysqldev1.ckb0c9p1c3xw.us-east-1.rds.amazonaws.com',
                },
                name: {
                    doc: 'Database name',
                    format: String,
                    default: 'vpmd',
                },
                username: {
                    doc: 'Database Username',
                    format: String,
                    default: 'awsdevmaster',
                },
                password: {
                    doc: 'Database Password',
                    format: String,
                    default: 'awsinfo2018',
                },
                pool: {
                    max: 5,
                    min: 0,
                    acquire: 30000,
                    idle: 10000,
                },
            },
            sqlite3: {
                dialect: 'sqlite3',
                connection: {
                    filename: 'vpdata.db',
                },
            },
        },
        SPRING_INBOX_FM: {
            mysql: {
                dialect: 'mysql',
                host: {
                    doc: 'Database host name/IP',
                    format: '*',
                    default: 'awsmysqldev1.ckb0c9p1c3xw.us-east-1.rds.amazonaws.com',
                },
                name: {
                    doc: 'Database name',
                    format: String,
                    default: 'devinboxfeaturemanagementappnew',
                },
                username: {
                    doc: 'Database Username',
                    format: String,
                    default: 'awsdevmaster',
                },
                password: {
                    doc: 'Database Password',
                    format: String,
                    default: 'awsinfo2018',
                },
                pool: {
                    max: 5,
                    min: 0,
                    acquire: 30000,
                    idle: 10000,
                },
            },
        },
    },
    links: {
        appeng_meta: __1.APPENG_META_URI,
    },
});
// Perform validation
config.validate({ allowed: 'strict' });
exports.default = config;
//# sourceMappingURL=index.js.map